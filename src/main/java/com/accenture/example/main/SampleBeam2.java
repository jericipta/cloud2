package com.accenture.example.main;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.PCollection;

import java.util.*;

public class SampleBeam2 {
    public interface PubSubToGcsOptions extends DataflowPipelineOptions {
        @Description("The Cloud Pub/Sub topic to read from.")
        @Validation.Required
        String getInputTopic();

        void setInputTopic(String value);

        @Description("Output file's window size in number of minutes.")
        @Default.Integer(1)
        Integer getWindowSize();

        void setWindowSize(Integer value);

        @Description("Path of the output file including its filename prefix.")
        @Validation.Required
        String getOutput();

        void setOutput(String value);
    }

    public static void main(String[] args) {
        sample1(args);
    }

    public static void sample1(String[] args) {

        // Build the table schema for the output table.
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("before_c1").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("before_c2").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("after_c1").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("after_c2").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("operation").setType("STRING"));
        fields.add(new TableFieldSchema().setName("timestamp").setType("TIMESTAMP"));
        TableSchema schema = new TableSchema().setFields(fields);

        PubSubToGcsOptions  options = PipelineOptionsFactory.fromArgs(args).withValidation()
                .as(PubSubToGcsOptions.class);
        options.setStreaming(true);

        String topic = "projects/" + options.getProject() + "/topics/postgres.public.sample5";

        String avgSpeedTable = options.getProject() + ":demos.sample1";

        Pipeline p = Pipeline.create(options);

        // Here is our workflow graph
        PCollection<TableRow> stream = p
                .apply("(1) Read PubSub Messages", PubsubIO.readStrings()
                        .fromTopic(topic))
                .apply("(2) Extract Json", ParDo.of(new DoFn<String, TableRow>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        Gson gson = new GsonBuilder().create();
                        HashMap<String, Object> parsedMap = gson.fromJson(c.element(), HashMap.class);
                        System.out.println("xxxxxxxx0 0 " + parsedMap.get("payload").toString());
                        HashMap<String, Object> payload = gson.fromJson(gson.toJson(parsedMap.get("payload")), HashMap.class);
                        String op = payload.get("op").toString();
                        Double ts_ms = (Double) payload.get("ts_ms");

                        HashMap<String, Object> pBefore = (payload.get("before") != null)
                                ? gson.fromJson(gson.toJson(payload.get("before")), HashMap.class)
                                : null ;
                        HashMap<String, Object> pAfter = (payload.get("after") != null)
                                ? gson.fromJson(gson.toJson(payload.get("after")), HashMap.class)
                                : null ;

                        TableRow row = new TableRow();
                        row.set("before_c1", (pBefore != null) ? pBefore.get("c1") : 0 );
                        row.set("before_c2", (pBefore != null) ? pBefore.get("c2") : 0 );
                        row.set("after_c1", (pAfter != null) ? pAfter.get("c1") : 0 );
                        row.set("after_c2", (pAfter != null) ? pAfter.get("c2") : 0 );
                        row.set("operation", op);
                        row.set("timestamp", ts_ms / 1000);
                        c.output(row);
                    }
                }));

        // We convert the PCollection to String so that we can write it to file
        stream.apply(BigQueryIO.writeTableRows().to(avgSpeedTable)//
                .withSchema(schema)//
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));


        // Finally we must run the pipeline, otherwise it's only a definition
        p.run().waitUntilFinish();
        System.out.println("IN 2");
    }
}
