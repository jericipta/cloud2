package com.accenture.example.main;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.Validation;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerTrxDataflow {
    public interface PubSubToGcsOptions extends DataflowPipelineOptions {
        @Description("The Cloud Pub/Sub topic to read from.")
        @Validation.Required
        String getInputTopic();

        void setInputTopic(String value);

        @Description("Output file's window size in number of minutes.")
        @Default.Integer(1)
        Integer getWindowSize();

        void setWindowSize(Integer value);

        @Description("Path of the output file including its filename prefix.")
        @Validation.Required
        String getOutput();

        void setOutput(String value);
    }

    public static void main(String[] args) {
        sample1(args);
    }

    public static void sample1(String[] args) {

        // Build the table schema for the output table.
        List<TableFieldSchema> fields = new ArrayList<>();

        fields.add(new TableFieldSchema().setName("b_ctrx").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_cid").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_id").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_date").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_ttl").setType("BIGINT"));
        fields.add(new TableFieldSchema().setName("b_trx_biaya").setType("BIGINT"));
        fields.add(new TableFieldSchema().setName("b_pym_method").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_no_va").setType("STRING"));

        fields.add(new TableFieldSchema().setName("b_ctrx").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_cid").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_id").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_date").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_trx_ttl").setType("BIGINT"));
        fields.add(new TableFieldSchema().setName("b_trx_biaya").setType("BIGINT"));
        fields.add(new TableFieldSchema().setName("b_pym_method").setType("STRING"));
        fields.add(new TableFieldSchema().setName("b_no_va").setType("STRING"));

        TableSchema schema = new TableSchema().setFields(fields);

        PubSubToGcsOptions  options = PipelineOptionsFactory.fromArgs(args).withValidation()
                .as(PubSubToGcsOptions.class);
        options.setStreaming(true);

        String topic = "projects/" + options.getProject() + "/topics/db2.db2inst1.transaction";

        String avgSpeedTable = options.getProject() + ":demos.customer_trx";

        Pipeline p = Pipeline.create(options);

        // Here is our workflow graph
        PCollection<TableRow> stream = p
                .apply("(1) Read PubSub Messages, Topic : db2.db2inst1.transaction ", PubsubIO.readStrings()
                        .fromTopic(topic))
                .apply("(2) Extract Json", ParDo.of(new DoFn<String, TableRow>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        Gson gson = new GsonBuilder().create();
                        HashMap<String, Object> parsedMap = gson.fromJson(c.element(), HashMap.class);
                        HashMap<String, Object> payload = gson.fromJson(gson.toJson(parsedMap.get("payload")), HashMap.class);
                        String op = payload.get("op").toString();
                        Double ts_ms = (Double) payload.get("ts_ms");

                        HashMap<String, Object> pBefore = (payload.get("before") != null)
                                ? gson.fromJson(gson.toJson(payload.get("before")), HashMap.class)
                                : null ;
                        HashMap<String, Object> pAfter = (payload.get("after") != null)
                                ? gson.fromJson(gson.toJson(payload.get("after")), HashMap.class)
                                : null ;

                        TableRow row = new TableRow();
                        row.set("b_ctrx", (pBefore != null) ? pBefore.get("ctrx".toUpperCase()) : "" );
                        row.set("b_cid", (pBefore != null) ? pBefore.get("cid".toUpperCase()) : "" );
                        row.set("b_trx_id", (pBefore != null) ? pBefore.get("trx_id".toUpperCase()) : "" );
                        row.set("b_trx_date", (pBefore != null) ? pBefore.get("trx_date".toUpperCase()) : "" );
                        row.set("b_trx_ttl", (pBefore != null) ? pBefore.get("trx_ttl".toUpperCase()) : 0);
                        row.set("b_trx_biaya", (pBefore != null) ? pBefore.get("trx_biaya".toUpperCase()) : 0 );
                        row.set("b_pym_method", (pBefore != null) ? pBefore.get("pym_method".toUpperCase()) : "" );
                        row.set("b_no_va", (pBefore != null) ? pBefore.get("no_va".toUpperCase()) : "" );

                        row.set("a_ctrx", (pAfter != null) ? pAfter.get("ctrx".toUpperCase()) : "" );
                        row.set("a_cid", (pAfter != null) ? pAfter.get("cid".toUpperCase()) : "" );
                        row.set("a_trx_id", (pAfter != null) ? pAfter.get("trx_id".toUpperCase()) : "" );
                        row.set("a_trx_date", (pAfter != null) ? pAfter.get("trx_date".toUpperCase()) : "" );
                        row.set("a_trx_ttl", (pAfter != null) ? pAfter.get("trx_ttl".toUpperCase()) : 0 );
                        row.set("a_trx_biaya", (pAfter != null) ? pAfter.get("trx_biaya".toUpperCase()) : 0 );
                        row.set("a_pym_method", (pAfter != null) ? pAfter.get("pym_method".toUpperCase()) : "" );
                        row.set("a_no_va", (pAfter != null) ? pAfter.get("no_va".toUpperCase()) : "" );

                        row.set("operation", op);
                        row.set("timestamp", ts_ms / 1000);
                        c.output(row);
                    }
                }));

        // We convert the PCollection to String so that we can write it to file
        stream.apply("(3) Write to BigQuery : " + args[0], BigQueryIO.writeTableRows().to(avgSpeedTable)//
                .withSchema(schema)//
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));


        // Finally we must run the pipeline, otherwise it's only a definition
        p.run().waitUntilFinish();
        System.out.println("IN 2");
    }
}
